NEW TURN IN INSTRUCTIONS FOR EVERY ASSIGNMENT ->
- In the locate directory I want a read_me.txt file with ->
    - a link to your online repository. 
    - A brief description of the following (if applicable for the given assignment):
        - An overview to your project
        - A brief summary of your thought process / approach when it came to tackling your assignment
        - Results (simple)
        - Issues you maybe having
        - How you improved upon your work, since the last week… 
            - Were can I see these changes.
        - Next steps for your project…
- Presentation PDF

So now you only need to drag and drop your assignments into the correct google drive folder. Also, follow this naming format for your assignment directories: FirstName_AssignmentName. 
And yes that means if you have a Project and a coding assignment I expect two read_me.txt files along with the relevant directories uploaded to the correct drive folder.
If a folder is not available for whatever reason. 
Email me the assignment(s) by whatever the deadline is…

Also as a reminder if you turn in a assignment late, yes that even means a minute past the deadline, you didn’t even bother completing / submitting the assignment in my book, so you get a ZERO. Also if I see that you have changed or in some way modified your Presentation / Code on Google Drive past the deadline (even a minute past the deadline), you get a ZERO for that assignment and didn’t turn it in. If for some reason your project / assignment takes a while to upload by the deadline and you miss the Deadline, ZERO; take that into account when submitting assignments.

No Exceptions, No excuses, Period.

Take responsibility and turn in your work on time, you guys are all Data Scientist now, and I’m going to treat you accordingly.

Say “acknowledged” or whatever you prefer when you read this…