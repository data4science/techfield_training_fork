

'''
Given a string s containing only open and close parenthesis.
What is the minimum number of parenthesis that needs to be flipped for
the string to become a set of balanced parenthesis.

Example
For s = "((", the output should be
flipForBalance(s) = 1.

Input/Output

[execution time limit] 4 seconds (py3)

[input] string s

A string of even non-empty length containing only '(' and ')'.

Guaranteed constraints:
2 ≤ s.length < 50.

[output] integer

The minimum number of parenthesis that needs to be flipped.

min = 0:
'()',
'(())',
'((()))', '(()())',
'(((())))', '((()()))', '(()()())'
min = 1:
'))', '((',
')())', '(()(', '()))', '((()'
min = 2:
')(',
')()(', '()()', '))))', '((((', ')(()', '())('
min = 3: ''
'''

def flipForBalance(s, count=0):
    if count == len(s) / 2:
        return('flipForBalance(string) = {1}'.format(count))
    
    for i in range(len(s) / 2):
    	if s[:i].pop != '(' and (len(s) == 2 or len(s) == 4):
    		count += 1
    		if s[i] != s[i - 1] or s[i + 1]:
    			flipForBalance(s, count)
    		else:
    			count += 1
    			flipForBalance(s, count)
    	if s[:i].pop == '(' and (len(s) == 2 or len(s) == 4):
    		flipForBalance(s, count)
    return('flipForBalance(string) = {1}'.format(count))