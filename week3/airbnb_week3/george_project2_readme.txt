https://bitbucket.org/data4science/techfield_training/src/b293264d46fc25e3e20d59aac66cb4a464cb7312/week2/airbnb_week2/?at=master

This project is based on the AirBnB data set of first time bookers destination country.

I approached this data set by working with the train csv file.  I reduced the feature size, and trained a SVC with a rmb kernnel.

I had an accuracy of 70+%.