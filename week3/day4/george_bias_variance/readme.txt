george_bias_variance

https://bitbucket.org/data4science/techfield_training/src/3fd15c67b9b84cb33c945de5e3d5330f8f2d27d3/week3/day4/george_bias_variance/?at=master

I answered the questions conceptually because I didn't have time to "get dirty" with the data.  Note: Conceptually understanding the correct answers to the questions without seeing concrete data displays a higher understanding of the concepts.